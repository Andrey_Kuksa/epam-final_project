package com.epam.web.entity;

public class User {
    private final String name;

    public String getName() {
        return name;
    }

    public User(String name) {
        this.name = name;
    }
}
